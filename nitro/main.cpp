//
//  main.cpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#include "InputOutput.hpp"

int main(int argc, const char * argv[]) {
    
    std::unique_ptr<InputOutput> io(new InputOutput());
    
    // Reads input json file.
    bool res = io->readJSON();
    
    if (res) {
        // If given json file has no syntax error.
        
        // find intersections of given shapes.
        io->findIntersections();
        
        // write results to an output.
        io->writeResultsToOutput();
    }

    return 0;
}
