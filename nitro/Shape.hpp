//
//  Rectangle.hpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#ifndef Shape_hpp
#define Shape_hpp

#include <set>
#include <string>

class Shape;
typedef std::shared_ptr<Shape> ShapePointer;

class Shape {
public:
    // a shape can be a single shape or intersection of shapes
    // ids hold the ids of all intersected shapes.
    // if it is an input shape, then it keeps only a single integer.
    std::set<int> ids;
    
    // name of a shape, defined in a JSON file.
    std::string name;

    // original name of a shape, displayed in an output file.
    std::string origName;

    // current attribute name. This is a placeholder to find out given value belongs to which attribute.
    std::string currAttrName;
        
    virtual ~Shape() = default;

    // intersection function to find whether the intersection exists and
    // if intersection exists, it will return the object of the intersected object.
    virtual std::pair<bool, ShapePointer> intersection(ShapePointer&) = 0;
    
    // function to use display properties of given shape.
    virtual std::string getProperties() = 0;
    
    // In some cases, JSON file may have no syntax error but has some semantics error.
    // For example, in one rectangle the "w" variable may not be declared.
    // The isValid function checks the given inputs are enough to create a shape.
    virtual bool isValid() = 0;
    
    // The given file attribute name may not be used for the specific shape.
    // For example, "z" attribute is not defined in a Rectangle shape.
    virtual bool attrNameExists(std::string) = 0;
    
    // Checks the input value is valid for the specific shape.
    virtual bool isValueValid(std::string) = 0;
    
    // Sets the value.
    virtual void setValue(std::string, std::string) = 0;
    
    // It will create new integer set for given two integer sets.
    // For example, the intersected shape may be a intersection of two intersected shapes.
    // Therefore, the new intersected shape should get
    std::set<int> combineIds(std::set<int>&, std::set<int>&);
};

// since there is no hash function for set of integers
// new hash function is created.
struct setofints_hash {
    std::size_t operator () (const std::set<int> &p) const {
        std::string str = "";
        
        for (auto it = p.begin(); it != p.end(); ++it) {
            str += std::to_string(*it) + "-";
        }
        str.pop_back();
        return std::hash<std::string>{}(str);
    }
};


#endif /* Shape_hpp */
