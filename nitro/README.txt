The source codes are implemented by Mert Duatepe on 10/09/2017.

The name of the header and source files:
State.hpp
State.cpp
Shape.hpp
Shape.cpp
Rectangle.hpp
Rectangle.cpp
FactoryShape.hpp
FactoryShape.cpp
InputOutput.hpp
InputOutput.cpp
main.cpp
input.json
README.txt

Most of the design decisions and details will be found in the comment lines of each class.

The important features that are used in the implementation are:
1) C++11 feaures such as shared_ptr, unique_ptr and override, default, auto keywords are used.
2) State Design Pattern and State Machines, to parse the JSON file.
3) Factory Design Pattern to create an object of given shape name.
4) Polymorphic declaration of Shape class to handle all shapes and its intersections.
5) I tried to do generic implementation for this assignment. Therefore, when we want to add a new Shape declaration, we only need to add new Derived class from Shape abstract class and add a few lines into FactoryShape class. No need to change InputOutput class, because it currently handles all the shapes and displays output in this way.

The one input file named "input.json" should be added into working directory. One sample input file added.

Build the code:
clang++ -std=c++11 -stdlib=libc++  -o nitro.o *.cpp

Run the code:
./nitro.o

After running nitro.o, a new file "output.txt" will automatically be created, displaying the results of given input.
