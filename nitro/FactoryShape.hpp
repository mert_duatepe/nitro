//
//  FactoryShape.hpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#ifndef FactoryShape_hpp
#define FactoryShape_hpp

#include "Shape.hpp"

// This class is used for creating objects from its names.
// And it will be used for determining the shape name exists or not.
class FactoryShape {
public:
    std::unique_ptr<Shape> getShapeObject(std::string);
    bool shapeNameExists(std::string);
};

#endif /* FactoryShape_hpp */
