//
//  InputOutput.cpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#include "InputOutput.hpp"

InputOutput::InputOutput() {
    machine = std::make_shared<JSON_Machine>();
    inputFileName = "input.json";
    outputFileName = "output.txt";
}

void InputOutput::writeResultsToOutput() {
    
    outFile.open(outputFileName);
    
    createIdVsStringIds();
    
    outFile << "Input: " << std::endl;
    for (int i = 0; i < startIndexOfIntersectedShapes; i++) {
        outFile << "\t" << (i + 1) << ": " << allShapes[i]->origName << " at " << allShapes[i]->getProperties() << "." << std::endl;
    }
    
    outFile << std::endl << "Intersections" << std::endl;
    int index = 1;
    for (int i = startIndexOfIntersectedShapes; i < allShapes.size(); i++) {
        index = 1;
        outFile << "\tBetween ";
        for (auto& nameVsIds : IdnameVSids[i]) {
            outFile << nameVsIds.first << " ";
            auto id = nameVsIds.second.begin();
            outFile << (*id);
            if (nameVsIds.second.size() > 1) {
                auto end = nameVsIds.second.end();
                --end;
                ++id;
                for (; id != end; ++id) {
                    outFile << ", " << (*id);
                }
                outFile << " and " << (*id);
            }
            if ( index != IdnameVSids[i].size() ) {
                outFile << ", and ";
            }
            index++;
        }
        outFile << " at " << allShapes[i]->getProperties() << "." << std::endl;
    }
    
    outFile.close();
}

void InputOutput::findIntersections() {

    startIndexOfIntersectedShapes = (int) allShapes.size();
    int first = 0;
    int end = startIndexOfIntersectedShapes;
    
    while(first < end) {
        for (int i = first; i < end; i++) {
            for (int j = 0; j < end; j++) {
                if (i == j)
                    continue;
                
                auto ids = allShapes[i]->combineIds(allShapes[i]->ids, allShapes[j]->ids);
                
                if (us.find(ids) != us.end())
                    continue;
                
                auto ret = allShapes[i]->intersection(allShapes[j]);
                if (ret.first) {
                    allShapes.push_back(ret.second);
                    us.insert(ret.second->ids);
                }
            }
        }
        first = end;
        end = (int) allShapes.size();
    }
}

bool InputOutput::readJSON() {
    
    inFile.open(inputFileName);
    
    if (!inFile) {
        outFile << "Unable to open input.json file." << std::endl;
        return false;
    }
    
    std::string in;
    int startIndex = 0;
    
    machine->setState(std::make_shared<StartState>());
    std::unique_ptr<State> endState(new EndState());

    // If getState is not end
    while (machine->getState()->stateName != "EndState" && inFile >> in ) {
        
        startIndex = 0;
        in.erase(remove_if(in.begin(), in.end(), isspace), in.end());
        
        while (startIndex < in.size()) {
            TransitionResult res = machine->nextStep(in, startIndex);
            startIndex = res.endIndex;
            
            if (!res.isSuccessful) {
                outFile.open(outputFileName);
                outFile << res.message << std::endl;
                outFile.close();
                inFile.close();
                return false;
            }
            
            if (res.objectCreated) {
                allShapes.push_back(res.obj);
            }
        }
    }
    
    if ( machine->getState()->stateName != "EndState" ) {
        outFile.open(outputFileName);
        outFile << "JSON File Error: Missing syntax in JSON file." << std::endl;
        outFile.close();
        inFile.close();
        return false;
    }
    
    inFile.close();
    return true;
}

void InputOutput::createIdVsStringIds()
{
    for(int i = 0; i < allShapes.size(); i++) {
        if (allShapes[i]->ids.size() == 1)
            continue;
        std::unordered_map<std::string, std::set<int>> newMap;
        for (auto id : allShapes[i]->ids) {
            newMap[allShapes[id - 1]->origName].insert(id);
        }
        IdnameVSids[i] = newMap;
        newMap.clear();
    }
}
