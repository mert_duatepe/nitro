//
//  State.cpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#include "State.hpp"

std::unique_ptr<FactoryShape> State::factory = std::unique_ptr<FactoryShape>(new FactoryShape());
std::unique_ptr<Shape> State::tempShape = nullptr;
std::string State::shapeName = "";
int State::shapeId = 1;

TransitionResult JSON_Machine::nextStep(std::string str, int startIndex) {
    return state->nextStep(shared_from_this(), str, startIndex);
}

void JSON_Machine::setState (std::shared_ptr<State> st) {
    state = st;
}

std::shared_ptr<State> JSON_Machine::getState() {
    return state;
}

TransitionResult EndState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;
    res.isSuccessful = false;
    res.message = "JSON File Error: Some characters after main block '}' ended";
    return res;
}

TransitionResult ShapePropertyBlockEndState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;
    
    if (str == "}" || str[startIndex] == '}') {
        machine->setState(std::make_shared<EndState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    else if (str == "," || str[startIndex] == ',') {
        machine->setState(std::make_shared<MainBlockStartState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    res.isSuccessful = false;
    res.message = "JSON File Error: ',' or '}' should be followed after Shape block.";
    return res;
}

TransitionResult ShapeAttributeBlockEndState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;
    
    // Ask temporary shape whether all attribute names and values are set.
    if ( ! State::tempShape->isValid() ) {
        res.isSuccessful = false;
        res.message = "JSON File Error: All attributes and values of the shape object does not set.";
        return res;
    }
    res.objectCreated = true;
    res.obj = std::move(State::tempShape);
    res.obj->ids.insert(shapeId++);
    
    if (str == "]" || str[startIndex] == ']') {
        machine->setState(std::make_shared<ShapePropertyBlockEndState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    if (str == "," || str[startIndex] == ',') {
        machine->setState(std::make_shared<ShapePropertyBlockStartState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    
    res.isSuccessful = false;
    res.message = "JSON File Error: ']' or ',' should be used when ending a Shape Property.";
    return res;
}

TransitionResult ShapeAttributeValueState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;
    
    if (str == "," || str[startIndex] == ',') {
        machine->setState(std::make_shared<ShapeAttributeBlockStartState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    else if (str == "}" || str[startIndex] == '}') {
        machine->setState(std::make_shared<ShapeAttributeBlockEndState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    res.isSuccessful = false;
    res.message = "JSON File Error: ',' or '}' should be followed after Shape attribute value.";
    return res;
}

TransitionResult ShapeAttributeNameColonState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;
    
    std::string val = "";
    int i = startIndex;

    for (; i < str.size(); i++) {
        if (str[i] >= '0' && str[i] <= '9')
            val += str[i];
        else if (str[i] == ',' || str[i] == '}') {
            break;
        }
        else {
            res.isSuccessful = false;
            res.message = "JSON File Error: Value is undefined.";
            return res;
        }
        
    }

    // whether the value is valid.
    if ( ! State::tempShape->isValueValid(val) ) {
        res.isSuccessful = false;
        res.message = "JSON File Error: Value is not valid.";
        return res;
    }
    State::tempShape->setValue(State::tempShape->currAttrName, val);
    machine->setState(std::make_shared<ShapeAttributeValueState>());
    res.isSuccessful = true;
    res.endIndex = i;
    return res;
}

TransitionResult ShapeAttributeNameState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;

    if (str == ":" || str[startIndex] == ':') {
        machine->setState(std::make_shared<ShapeAttributeNameColonState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    res.isSuccessful = false;
    res.message = "JSON File Error: ':' should be followed after Shape attribute name.";
    return res;
}

TransitionResult ShapeAttributeBlockStartState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;

    std::string name = "";
    
    if (str[startIndex] != '"') {
        res.isSuccessful = false;
        res.message = "JSON File Error: Shape attribute name should start with '""'.";
        return res;
    }
    
    int i = startIndex + 1;
    for (; i < str.size(); i++) {
        if (str[i] != '"') {
            name += str[i];
        }
        else {
            i++;
            break;
        }
    }

    // Ask temp shape whether the attribute name exists.
    if ( ! State::tempShape->attrNameExists(name) ) {
        res.isSuccessful = false;
        res.message = "JSON File Error: Shape attribute name does not exist.";
        return res;
    }
    
    State::tempShape->currAttrName = name;
    machine->setState(std::make_shared<ShapeAttributeNameState>());
    res.isSuccessful = true;
    res.endIndex = i;
    return res;
}


TransitionResult ShapePropertyBlockStartState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;
    
    if (str == "{" || str[startIndex] == '{') {
        State::tempShape = State::factory->getShapeObject(State::shapeName);
        machine->setState(std::make_shared<ShapeAttributeBlockStartState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    res.isSuccessful = false;
    res.message = "JSON File Error: '{' should be used when starting a Shape attribute names and values.";
    return res;
}


TransitionResult ShapeNameColonState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;

    if (str == "[" || str[startIndex] == '[') {
        machine->setState(std::make_shared<ShapePropertyBlockStartState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    res.isSuccessful = false;
    res.message = "JSON File Error: '[' should be used when starting a Shape Property.";
    return res;
}


TransitionResult ShapeNameState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;

    if (str == ":" || str[startIndex] == ':') {
        machine->setState(std::make_shared<ShapeNameColonState>());
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        return res;
    }
    res.isSuccessful = false;
    res.message = "JSON File Error: ':' should be followed after the name of the Shape.";
    return res;
}


TransitionResult MainBlockStartState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {

    std::string name = "";
    TransitionResult res;
    
    if (str[startIndex] != '"') {
        res.isSuccessful = false;
        res.message = "JSON File Error: Shape name should start with '""'.";
        return res;
    }
    
    int i = startIndex + 1;
    for (; i < str.size(); i++) {
        if (str[i] != '"') {
            name += str[i];
        }
        else {
            i++;
            break;
        }
    }
    
    // Factory class: Get info that the shape name exists.
    if ( ! State::factory->shapeNameExists(name) ) {
        res.isSuccessful = false;
        res.message = "JSON File Error: Shape name does not exist.";
        return res;
    }
    
    State::shapeName = name;

    machine->setState(std::make_shared<ShapeNameState>());
    res.isSuccessful = true;
    res.endIndex = i;
    return res;
}

TransitionResult StartState::nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) {
    TransitionResult res;

    if (str == "{" || str[startIndex] == '{')
    {
        res.isSuccessful = true;
        res.endIndex = startIndex + 1;
        machine->setState(std::make_shared<MainBlockStartState>());
        return res;
    }
    res.isSuccessful = false;
    res.message = "JSON File Error: Main block should start with '{'.";
    return res;
}
