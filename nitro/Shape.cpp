//
//  Rectangle.cpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#include "Shape.hpp"

std::set<int> Shape::combineIds(std::set<int>& f, std::set<int>& s) {
    std::set<int> total_ids;
    
    for (auto it = f.begin(); it != f.end(); ++it) {
        total_ids.insert(*it);
    }

    for (auto it = s.begin(); it != s.end(); ++it) {
        total_ids.insert(*it);
    }
    
    return total_ids;
}
