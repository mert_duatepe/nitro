//
//  Rectangle.hpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#ifndef Rectangle_hpp
#define Rectangle_hpp

#include "Shape.hpp"
#include <regex>
#include <set>
#include <sstream>


class Rectangle;
typedef std::shared_ptr<Rectangle> RectPointer;

// (X,Y) --> W
//   |
//   v
//   H

// A rectangle is an intersection of 4 lines
// 2 of them are horizontal lines
// 2 of them are vertical lines
// x1 is the vertical line 1 (X)
// x2 is the vertical line 2 (X + W)
// y1 is the horizontal line 1 (Y)
// y2 is the horizontal line 2 (Y + H)
class Rectangle : public Shape {
private:
    void calculateX2andY2();
public:
    int x1;
    int y1;
    int x2;
    int y2;
    int h;
    int w;
    Rectangle() { origName = "Rectangle"; name = "rects"; x1=0; y1=0; x2=0; y2=0; h=0; w=0; }
    Rectangle(int,int,int,int,int);
    Rectangle(std::set<int>,int,int,int,int);
    virtual std::pair<bool, ShapePointer> intersection(ShapePointer&);
    virtual std::string getProperties();
    virtual bool isValid();
    virtual bool attrNameExists(std::string);
    virtual bool isValueValid(std::string);
    virtual void setValue(std::string, std::string);
};


#endif /* Rectangle_hpp */
