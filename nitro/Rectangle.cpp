//
//  Rectangle.cpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#include "Rectangle.hpp"

Rectangle::Rectangle(int pos, int in_x1, int in_y1, int in_x2, int in_y2) : Rectangle() {
    ids.insert(pos);
    this->x1 = in_x1;
    this->x2 = in_x2;
    this->y1 = in_y1;
    this->y2 = in_y2;
}

Rectangle::Rectangle(std::set<int> ids, int in_x1, int in_y1, int in_x2, int in_y2) : Rectangle() {
    this->ids = ids;
    this->x1 = in_x1;
    this->x2 = in_x2;
    this->y1 = in_y1;
    this->y2 = in_y2;
}

std::string Rectangle::getProperties() {
    std::string property = "(" + std::to_string(x1) + "," + std::to_string(y1) + "), " +
    "w=" + std::to_string(x2 - x1) + ", " + "h=" + std::to_string(y2 - y1);
    
    return property;
}


bool Rectangle::attrNameExists(std::string attrName) {
    if (attrName == "x" || attrName == "y" || attrName == "w" || attrName == "h") {
        return true;
    }
    return false;
}

bool Rectangle::isValueValid(std::string value) {
    std::regex r("[[:digit:]]+");
    
    if(regex_match(value, r))
        return true;
    
    return false;
}

void Rectangle::calculateX2andY2() {
    x2 = x1 + w;
    y2 = y1 + h;
}

bool Rectangle::isValid() {
    if (x1 <= 0 || y1 <= 0 || w <= 0 || h <= 0)
        return false;
    calculateX2andY2();
    return true;
}

void Rectangle::setValue(std::string name, std::string strVal)
{
    int val = 0;
    if ( ! (std::istringstream(strVal) >> val) )
        val = 0;
    
    if (name == "x") {
        x1 = val;
    }
    if (name == "y") {
        y1 = val;
    }
    if (name == "w") {
        w = val;
    }
    if (name == "h") {
        h = val;
    }
}

std::pair<bool, ShapePointer> Rectangle::intersection(ShapePointer& shape) {
    
    if ( RectPointer r = std::dynamic_pointer_cast<Rectangle>(shape) ) {
        int x3 = std::max(x1, r->x1);
        int y3 = std::max(y1, r->y1);
        int x4 = std::min(x2, r->x2);
        int y4 = std::min(y2, r->y2);
        
        if ( (x3 >= x4) || (y3 >= y4) ) {
            return std::make_pair(false, nullptr);
        }
        
        auto intersectedRectIds = combineIds(this->ids, shape->ids);
                
        return make_pair(true, std::make_shared<Rectangle>(intersectedRectIds, x3, y3, x4, y4));
    }
    return std::make_pair(false, nullptr);
}
