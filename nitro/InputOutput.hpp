//
//  InputOutput.hpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#ifndef InputOutput_hpp
#define InputOutput_hpp

#include <iostream>
#include <vector>
#include <unordered_set>
#include <set>
#include <unordered_map>
#include <iterator>
#include <fstream>

#include "State.hpp"
#include "Shape.hpp"

class InputOutput {
private:
    // Keeps all input shapes and all the intersected shapes in a vector of ShapePointers.
    std::vector<ShapePointer> allShapes;
    
    // JSON file deterministic finite state machine.
    std::shared_ptr<JSON_Machine> machine;
    
    // Keeps all shape ids in a hash set of sets.
    // For example, if input and intersected shapes are such as:
    // 1. Rectangle (ids: set of <1>)   - input rectangle
    // 2. Rectangle (ids: set of <2>)   - input rectangle
    // 3. Rectangle (ids: set of <1,2>) - intersected rectangle
    // Then this hash set will keep the values such as < <1>, <2>, <1,2> >
    // This structure will be used, in order to find out whether the intersection of rectangles has computed before.
    std::unordered_set<std::set<int>, setofints_hash> us;
    
    // Some shapes in the allShapes structure are input shapes,
    // the others are the intersected shapes
    // For the intersected shapes, we need to keep track of all the shape names and ids that constitutes the intersected shape.
    // For example, < 1, map< <"Rectangle", set<2,3> > >
    // Above example shows that the Rectangle 1 is the intersection of rectangles of 2 and 3.
    std::unordered_map<int, std::unordered_map<std::string, std::set<int>>> IdnameVSids;
    
    // Since all input shapes and all the intersected shapes are kept in a vector of ShapePointers,
    // the start index of the intersected shapes within the allShapes is kept in the following value.
    int startIndexOfIntersectedShapes;
    
    // Input file name and file handle.
    std::string inputFileName;
    std::ifstream inFile;

    // Output file name and file handle.
    std::string outputFileName;
    std::ofstream outFile;
    
    // After collecting the input shapes and calculating the intersected shapes
    // the following function will create the structure of IdnameVSids
    // in order to display the output to given format.
    void createIdVsStringIds();

public:
    InputOutput();
    
    bool readJSON();
    void findIntersections();
    void writeResultsToOutput();
};


#endif /* InputOutput_hpp */
