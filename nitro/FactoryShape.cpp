//
//  FactoryShape.cpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#include "FactoryShape.hpp"
#include "Rectangle.hpp"

std::unique_ptr<Shape> FactoryShape::getShapeObject(std::string str) {
    
    if (str == "rects")
        return std::unique_ptr<Shape>(new Rectangle());
    
    return nullptr;
}

bool FactoryShape::shapeNameExists(std::string str) {
    
    if (str == "rects")
        return true;
    
    return false;
}
