//
//  State.hpp
//  nitro
//
//  Created by Mert Duatepe on 10/09/2017.
//  Copyright © 2017 Mert Duatepe. All rights reserved.
//

#ifndef State_hpp
#define State_hpp

#include <string>
#include "FactoryShape.hpp"
#include "Shape.hpp"

class State;

// In each state transition, there will always be a result.
// This result will be returned with the following structure.
struct TransitionResult {
public:
    bool isSuccessful;
    int endIndex;
    std::string message;
    bool objectCreated;
    std::shared_ptr<Shape> obj;
    TransitionResult() { isSuccessful = true; message = ""; endIndex = 0; objectCreated = false; obj = nullptr; }
};

class JSON_Machine : public std::enable_shared_from_this<JSON_Machine> {
private:
    std::shared_ptr<State> state;
public:
    JSON_Machine() = default;
    void setState (std::shared_ptr<State>);
    std::shared_ptr<State> getState();
    TransitionResult nextStep(std::string, int);
};

// Abstract base class for states.
class State {
public:
    State(std::string name) { stateName = name; }
    
    // Each state has an nextStep function,
    // to determine which state should be transitioned with given input string.
    virtual TransitionResult nextStep(std::shared_ptr<JSON_Machine>, std::string, int) = 0;
    
    // Factory object to return shape object from given name,
    // it should be only one factory object in the entire program.
    static std::unique_ptr<FactoryShape> factory;
    
    // Temporary shape object to keep track of the shape object during state transitions
    // it should be only one temporary object during state transitions.
    // When a new shape definition is started, the temporary object will be written into vector of ShapePointers.
    static std::unique_ptr<Shape> tempShape;

    // shapeName string to keep the name of the shape.
    static std::string shapeName;
    
    // Id should be assigned when adding temporary shape object into ShapePointers.
    // It will keep the next available shape Id.
    static int shapeId;
    
    // Name of the state.
    std::string stateName;
};


// All state definitions to check JSON sytnax is correct
// and gather shape information and its properties from JSON file.
class EndState : public State {
public:
    EndState() : State("EndState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};

class ShapePropertyBlockEndState : public State {
public:
    ShapePropertyBlockEndState() : State("ShapePropertyBlockEndState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};

class ShapeAttributeBlockEndState : public State {
public:
    ShapeAttributeBlockEndState() : State("ShapeAttributeBlockEndState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};

class ShapeAttributeValueState : public State {
public:
    ShapeAttributeValueState() : State("ShapeAttributeValueState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};

class ShapeAttributeNameColonState : public State {
public:
    ShapeAttributeNameColonState() : State("ShapeAttributeNameColonState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};

class ShapeAttributeNameState : public State {
public:
    ShapeAttributeNameState() : State("ShapeAttributeNameState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};

class ShapeAttributeBlockStartState : public State {
public:
    ShapeAttributeBlockStartState() : State("ShapeAttributeBlockStartState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};


class ShapePropertyBlockStartState : public State {
public:
    ShapePropertyBlockStartState() : State("ShapePropertyBlockStartState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};


class ShapeNameColonState : public State {
public:
    ShapeNameColonState() : State("ShapeNameColonState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};


class ShapeNameState : public State {
public:
    ShapeNameState() : State("ShapeNameState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};


class MainBlockStartState : public State {
public:
    MainBlockStartState() : State("MainBlockStartState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};

class StartState : public State {
public:
    StartState() : State("StartState") {}
    TransitionResult nextStep(std::shared_ptr<JSON_Machine> machine, std::string str, int startIndex) override;
};


#endif /* State_hpp */
